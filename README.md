# Round Robin and FCFS Algorithm Visualizer (OS)
[Here is the website for this repository](http://roundrobinos.netlify.app)
## Always add the Original 😁
This code is a tweaked version of the
[Original Code in Codepen](https://codepen.io/faso/pen/zqWGQW)

## Added Features and Tweaks
- Arrival time is now entered by the user
- It is now possible to detect and include the times that the "CPU" is Idle (has no process to execute)
- Calcualtion of all waiting times and average waiting time
- Animation is now Optional (took too much time in case of too many processes 😬)

