
$(document).ready(function () {

  $('input[type=radio][name=algorithm]').change(function () {

    if (this.value == 'robin') {
      $('.servtime').hide();
      $('#quantumParagraph').show();
    }
    else {
      $('#quantumParagraph').hide();
      $('.servtime').show();
    }

  });
});

function addRow() {
  var lastRow = $('#inputTable tr:last');
  console.log(lastRow);
  console.log(lastRow.children());
  var lastRowNumebr = parseInt(lastRow.children()[0].innerText[1]);

  var newRow = '<tr><td>P'
    + (lastRowNumebr + 1)
    + '</td><td>'
    + '<input class="arrtime" type="text" value="0"/></td><td>'
    + '<input class="exectime" type="text" value="1"/></td>';
  //if ($('input[name=algorithm]:checked', '#algorithm').val() == "priority")

  lastRow.after(newRow);

  var minus = $('#minus');
  minus.show();
  minus.css('top', (parseFloat(minus.css('top')) + 24) + 'px');

  if ($('input[name=algorithm]:checked', '#algorithm').val() != "priority")
    $('.priority-only').hide();


}

function deleteRow() {

  var lastRow = $('#inputTable tr:last');
  lastRow.remove();

  var minus = $('#minus');

  if ($('#inputTable tr').length == 2)
    minus.hide();
}




function animate(animate) {
  if (animate) {
    $('fresh').prepend('<div id="curtain" style="position: absolute; right: 0; width:100%; height:100px;"></div>');

    $('#curtain').width($('#resultTable').width());
    $('#curtain').css({ left: $('#resultTable').position().left });
  }
  var sum = 0;
  const tables = $('#resultTable tr:last td');

  $.each(tables, (key, value) => {
    console.log($(value).text());
    sum += Number($(value).text());
  });

  var distance = $("#curtain").css("width");
  if(animate){
  animationStep(sum, 0);
  jQuery('#curtain').animate({ width: '0', marginLeft: distance }, sum * 1000 / 2, 'linear');
  }else{
    $('#timer').html(sum);
  }
}

function animationStep(steps, cur) {
  $('#timer').html(cur);
  if (cur < steps) {
    setTimeout(function () {
      animationStep(steps, cur + 1);
    }, 500);
  }
  else {
  }
}

function draw() {
  $('fresh').html('');
  $('#waitingTable').html('');
  let inputTable = $('#inputTable tr');
  let th = '';
  let td = '';
  let algorithm = $('input[name=algorithm]:checked', '#algorithm').val();
  let quantum = 0;
  let executeTimes = [];

  $.each(inputTable, function (key, value) {
    if (key == 0) return true;
    const executeTime = parseInt($(value.children[2]).children().first().val());
    const arrivalTime = parseInt($(value.children[1]).children().first().val());

    executeTimes[key - 1] = { "realExecuteTime": executeTime, "executeTime": executeTime, "P": key - 1, 'arrivalTime': arrivalTime, 'lastExecute': arrivalTime };
  });

  if (algorithm == 'fcfs') {
    quantum = Math.max(inputTable.map(item => item.executeTime));
  } else {
    quantum = $('#quantum').val();
  }

  executeTimes.sort((a, b) => a['arrivalTime'] - b['arrivalTime']);
  console.log(executeTimes);
  let time = 0;
  let areWeThereYet = false;
  let stuck = false;
  let minKey = 0;
  let value = executeTimes[minKey];
  while (!areWeThereYet) {
    areWeThereYet = true;
    const tmp = value;
    value = { 'lastExecute': Infinity };
    if (stuck) {
      for (let i = 0; i < executeTimes.length; i++) {
        console.log('STUCK');
        if (executeTimes[i].lastExecute < value.lastExecute && executeTimes[i].executeTime > 0 && executeTimes[i].arrivalTime - time <= 0) {
          value = executeTimes[i];
          console.log(value);
          console.log(value);
        }
      }
      if (value.lastExecute == Infinity) {
        value = tmp;
      }
    } else {
      for (let i = 0; i < executeTimes.length; i++) {
        console.log('NON STUCK');
        if (executeTimes[i].lastExecute < value.lastExecute && executeTimes[i].executeTime > 0) {

          value = executeTimes[i];
          console.log(value);

          console.log(value);
        }
      }
    }

    if (value.executeTime > 0) {
      let remainingToArrival = value.arrivalTime - time;

      if (remainingToArrival > 0) {
        if (stuck) {

          th += '<th style="height: 60px; width: ' + remainingToArrival * 20 + 'px;">Idle</th>';
          td += '<td>' + remainingToArrival + '</td>';
        } else {
          stuck = true;
          areWeThereYet = false;
          continue;
        }
      }
      stuck = false;


      const finalExecuteTime = Number(value.executeTime > quantum ? quantum : value.executeTime);
      th += '<th style="height: 60px; width: ' + (finalExecuteTime) * 20 + 'px;">P' + value.P + '</th>';
      td += '<td>' + (value.executeTime > quantum ? quantum : value.executeTime) + '</td>';
      if (remainingToArrival < 0) {
        remainingToArrival = 0;
      }
      time += remainingToArrival + finalExecuteTime;


      value.lastExecute = time;
      console.log(time);
      
      value.executeTime -= quantum;
      areWeThereYet = false;
    }

  }

  let waitTable = '</br><table><tr><th>Process</th><th>Waiting Time</th></tr>';
  let sum = 0;
  executeTimes.sort((a, b) => a.P - b.P);
  $.each(executeTimes, (_, value) => {
    const lastExecute = Number(value.lastExecute);
    const arrivalTime = Number(value.arrivalTime);
    const executeTime = Number(value.realExecuteTime);
    const waitingTime = lastExecute - arrivalTime - executeTime;
    sum += waitingTime;
    console.log(lastExecute + ' ' + arrivalTime + ' ' + executeTime);
    const waitTableTMP = '<tr> <td> P' + value.P + ' </td> <td> ' + (waitingTime) + ' </td> </tr>';
    waitTable += waitTableTMP;
  });

  waitTable += '</table>  <div> <p></br> Average Wait Time: <strong>' + (sum / executeTimes.length) + ' </strong> sec </p> <div>';
  $('#waitingTable').html(waitTable);

  $('fresh').html('<table id="resultTable" style="width: 70%"><tr>'
    + th
    + '</tr><tr>'
    + td
    + '</tr></table>'
  );
  animate($('#animation').is(':checked'));
}
